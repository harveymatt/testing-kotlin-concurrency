## Playing with Kotlin to test concurrency 

I want to understand how Kotlin's concurrency model.

### TL;DR
- Run the Server `gradlew bootRun`
- Run a basic load test `ab -n 30 -c 15 http://localhost:8080/users/delay`

### History
1. Used [Spring initializer](https://start.spring.io/) to get it running.
2. Setup a basic controller to test `Thread.sleep` vs `suspend` functions
3. Started running tests

### Current observation
15 concurrent requests seems like it should be easy. But I'm getting connection resets??
```ab -s 999 -n 15 -c 15 http://localhost:8080/users/delay
This is ApacheBench, Version 2.3 <$Revision: 1901567 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)...apr_socket_recv: Connection reset by peer (54)```
