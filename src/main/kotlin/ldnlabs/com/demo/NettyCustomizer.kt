package ldnlabs.com.demo

import io.netty.channel.EventLoopGroup
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory
import org.springframework.boot.web.embedded.netty.NettyServerCustomizer

import org.springframework.boot.web.server.WebServerFactoryCustomizer
import org.springframework.stereotype.Component
import java.util.concurrent.Executors

@Component
class NettyWebServerFactoryPortCustomizer : WebServerFactoryCustomizer<NettyReactiveWebServerFactory> {
    override fun customize(serverFactory: NettyReactiveWebServerFactory) {
        serverFactory.addServerCustomizers(nettyServerCustomizer())
    }
}

fun nettyServerCustomizer(): NettyServerCustomizer {
    return NettyServerCustomizer { httpServer ->
        val eventLoopGroup: EventLoopGroup = NioEventLoopGroup(1, Executors.newFixedThreadPool(1))
        eventLoopGroup.register(NioServerSocketChannel())
        httpServer.runOn(eventLoopGroup)
    }
}
