package ldnlabs.com.demo.users

import kotlinx.coroutines.delay
import org.springframework.scheduling.annotation.Async
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController()
@RequestMapping("users")
class UserController {

    data class User(val name: String)

    @GetMapping(
       value = ["/delay"],
       produces = ["application/json"]
    )
    suspend fun getDelayedUser(@RequestParam(value="name", defaultValue="Jane" ) name: String): User {
        delay(10000)
        return User(name)
    }


    @GetMapping(
        value = ["/sleep"],
        produces = ["application/json"]
    )
    fun getSleepyUser(@RequestParam(value="name", defaultValue="Jane" ) name: String): User {
        Thread.sleep(10000)
        return User(name)
    }
}